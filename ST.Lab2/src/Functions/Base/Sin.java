package Functions.Base;

import Functions.*;
import Infrastructure.*;

public class Sin implements IFunction
{
    @Override
    public double Calculate(double x)
    {
        double tolerance = Settings.tolerance;
        double result, previousValue;
        double termFactor = (-1) * Math.pow(x, 2);
        double term = x;
        int n = 3;
        result = term;
        do   
        {
            previousValue = result;
            term  *= termFactor / (n *(n-1));
            result += term;
            n +=2;
        }
        while (tolerance <= Math.abs(result - previousValue));
        if (Settings.needWriteSin)
        {
            CsvWriter.Write(Settings.sinName, x, result);
        }
        return result;
    }    
}
