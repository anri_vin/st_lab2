package Functions.Base;

import Functions.IFunction;
import Infrastructure.CsvWriter;
import Infrastructure.Settings;

public class NaturalLogarithm implements IFunction
{
    @Override
    public double Calculate(double argument) 
    {
        if (argument == 0) { return Double.NEGATIVE_INFINITY; }
        if (argument < 0) { return Double.NaN; }
        double x = (argument )/(argument - 1);
        double result, previousValue;
        double termFactor =  1/x;
        double term = 1/x;
        result = term;
        int n = 2;
        do
        {
            previousValue = result;
            term *= termFactor ;
            result += term / n;
            n++;
        }
        while (Settings.tolerance <= Math.abs(result - previousValue));
        if (Settings.needWriteLn)
        {
            CsvWriter.Write(Settings.lnName, argument, result);
        }
        return result;
    } 
}
