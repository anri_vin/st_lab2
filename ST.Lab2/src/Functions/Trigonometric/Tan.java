package Functions.Trigonometric;

import Functions.Base.Sin;
import Functions.IFunction;
import Infrastructure.CsvWriter;
import Infrastructure.Settings;

public class Tan implements IFunction {
    private final IFunction sin;
    private final IFunction cos;
    
    public Tan() {
        sin = new Sin();
        cos = new Cos();
    }
    
    public Tan(IFunction sin) {
        this.sin = sin;
        cos = new Cos(sin);
    }
    
    @Override
    public double Calculate(double x) {
        double result = sin.Calculate(x) / cos.Calculate(x);
        if (Settings.needWriteTan)
        {
            CsvWriter.Write(Settings.tanName, x, result);
        }
        return result;
    }
}
