package Functions.Trigonometric;

import Functions.Base.Sin;
import Functions.IFunction;
import Infrastructure.CsvWriter;
import Infrastructure.Settings;

public class Cot implements IFunction {
    private final IFunction sin;
    private final IFunction cos;
    
    public Cot() {
        sin = new Sin();
        cos = new Cos();
    }
    
    public Cot(IFunction sin) {
        this.sin = sin;
        cos = new Cos(sin);
    }
    
    @Override
    public double Calculate(double x) {
        double result = cos.Calculate(x) / sin.Calculate(x);
        if (Settings.needWriteCot)
        {
            CsvWriter.Write(Settings.cotName, x, result);
        }
        return result;
    }
}
