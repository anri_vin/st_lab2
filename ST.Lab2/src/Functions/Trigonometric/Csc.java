package Functions.Trigonometric;

import Functions.Base.Sin;
import Functions.IFunction;
import Infrastructure.CsvWriter;
import Infrastructure.Settings;

public class Csc implements IFunction {
    private final IFunction sin;
    
    public Csc() {
        sin = new Sin();
    }
    
    public Csc(IFunction sin) {
        this.sin = sin;
    }
    
    @Override
    public double Calculate(double x) {
        double result = 1 / sin.Calculate(x);
        if (Settings.needWriteCsc)
        {
            CsvWriter.Write(Settings.cscName, x, result);
        }
        return result;
    }
}
