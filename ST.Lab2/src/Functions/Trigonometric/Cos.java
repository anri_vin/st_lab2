package Functions.Trigonometric;

import Functions.Base.Sin;
import Functions.IFunction;
import Infrastructure.CsvWriter;
import Infrastructure.Settings;

public class Cos implements IFunction{
    
    private final IFunction sin;
    
    public Cos(){
        sin = new Sin();
    }
    
    public Cos(IFunction sin)
    {
        this.sin = sin;
    }
    
    @Override
    public double Calculate(double x) {
        double result = Math.pow(1 - Math.pow(sin.Calculate(x), 2), 0.5);
        result *= getSign(x);
        if (Settings.needWriteCos)
        {
            CsvWriter.Write(Settings.cosName, x, result);
        }
        return result;
    }
    
    private int getSign(double x)
    {
        int xSign = (x < 0) ? -1: 1; 
        int periodNumber = (int)(Math.abs(x) / (2 * Math.PI));
        double periodFirstPoint = xSign * Math.PI * (2 * periodNumber + 1) - (Math.PI / 2);
        double periodSecondPoint = xSign * Math.PI * (2 * periodNumber + 1) + (Math.PI / 2);
        if (x > periodFirstPoint && x < periodSecondPoint)
            return -1;
        else return 1;
                //return (x  + (Math.PI / 2.0)<= periodMiddle) ? 1: -1;
    }
}
