package Functions;

import Functions.Base.NaturalLogarithm;
import Functions.Logarithmic.*;
import Infrastructure.CsvWriter;
import Infrastructure.Settings;

public class LogarithmicFunction implements IFunction
{
    private final IFunction log10;
    private final IFunction log5;
    private final IFunction log2;
    private final IFunction ln;
    
    
    public LogarithmicFunction()
    {
        log10 = new LogarithmBase10();
        log5 = new LogarithmBase5();
        log2 = new LogarithmBase2();
        ln = new NaturalLogarithm();
    }
    
    public LogarithmicFunction(IFunction log10,
                         IFunction log5,
                         IFunction log2,
                         IFunction ln)
    {
        this.log10 = log10;
        this.log5 = log5;
        this.log2 = log2;
        this.ln = ln;
    }

    @Override
    public double Calculate(double x) 
    {
        double log10X = log10.Calculate(x);
        double log5X = log5.Calculate(x);
        double log2X = log2.Calculate(x);
        double lnX = ln.Calculate(x);
        double result = ((Math.pow(log2X * log5X, 3) * lnX) + (lnX * (log5X + log10X))) + log2X;
        if (Settings.needWriteLogarithmicFunc)
        {
            CsvWriter.Write(Settings.logarithmicFuncName, x, result);
        }
        return result;
    }
    
}
