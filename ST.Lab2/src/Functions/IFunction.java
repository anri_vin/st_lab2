package Functions;

public interface IFunction {
    
    double Calculate(double x);
    
}
