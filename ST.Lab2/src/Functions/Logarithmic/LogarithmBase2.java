package Functions.Logarithmic;

import Functions.Base.NaturalLogarithm;
import Functions.IFunction;
import Infrastructure.CsvWriter;
import Infrastructure.Settings;

public class LogarithmBase2 implements IFunction
{
    private final IFunction ln;
    
    public LogarithmBase2()
    {
        ln = new NaturalLogarithm();
    }
    
    public LogarithmBase2(IFunction naturalLog)
    {
        ln = naturalLog;
    }

    @Override
    public double Calculate(double x) 
    {
        double result = ln.Calculate(x) / ln.Calculate(2);
        if (Settings.needWriteLog2)
        {
            CsvWriter.Write(Settings.log2Name, x, result);
        }
        return result;
    }    
}
