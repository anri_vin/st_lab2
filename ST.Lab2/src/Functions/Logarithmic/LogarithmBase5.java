package Functions.Logarithmic;

import Functions.Base.NaturalLogarithm;
import Functions.IFunction;
import Infrastructure.CsvWriter;
import Infrastructure.Settings;

public class LogarithmBase5 implements IFunction
{
    private final IFunction ln;
    
    public LogarithmBase5()
    {
        ln = new NaturalLogarithm();
    }
    
    public LogarithmBase5(IFunction naturalLog)
    {
        ln = naturalLog;
    }

    @Override
    public double Calculate(double x) 
    {
        double result = ln.Calculate(x) / ln.Calculate(5);
        if (Settings.needWriteLog5)
        {
            CsvWriter.Write(Settings.log5Name, x, result);
        }
        return result;
    }    
}
