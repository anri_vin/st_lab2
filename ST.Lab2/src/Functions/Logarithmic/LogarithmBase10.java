package Functions.Logarithmic;

import Functions.Base.NaturalLogarithm;
import Functions.IFunction;
import Infrastructure.CsvWriter;
import Infrastructure.Settings;

public class LogarithmBase10 implements IFunction
{
    private final IFunction ln;
    
    public LogarithmBase10()
    {
        ln = new NaturalLogarithm();
    }
    
    public LogarithmBase10(IFunction naturalLog)
    {
        ln = naturalLog;
    }

    @Override
    public double Calculate(double x) 
    {
        double result = ln.Calculate(x) / ln.Calculate(10);
        if (Settings.needWriteLog10)
        {
            CsvWriter.Write(Settings.log10Name, x, result);
        }
        return result;
    }    
}
