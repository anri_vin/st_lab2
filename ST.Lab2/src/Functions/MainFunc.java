package Functions;

import Infrastructure.CsvWriter;
import Infrastructure.Settings;

public class MainFunc implements IFunction
{
    private final IFunction logarithmicFunction;
    private final IFunction trigonometricFunction;
    
    
    public MainFunc()
    {
        logarithmicFunction = new LogarithmicFunction();
        trigonometricFunction = new TrigonometricFunction();
    }
    
    public MainFunc(IFunction logarithmFunc, IFunction trigonometricFunc)
    {
        logarithmicFunction = logarithmFunc;
        trigonometricFunction = trigonometricFunc;
    }

    @Override
    public double Calculate(double x) 
    {
        double result = (x <= 4) ? trigonometricFunction.Calculate(x) : logarithmicFunction.Calculate(x);
        if (Settings.needWriteMainFunc)
        {
            CsvWriter.Write(Settings.mainFuncName, x, result);
        }
        return result;
    }
    
}
