package Functions;

import Functions.Base.Sin;
import Functions.Trigonometric.*;
import Infrastructure.CsvWriter;
import Infrastructure.Settings;


public class TrigonometricFunction implements IFunction
{
    private final IFunction sin, cos, tan, cot, csc;
    
    public TrigonometricFunction() {
        sin = new Sin();
        cos = new Cos();
        tan = new Tan();
        cot = new Cot();
        csc = new Csc();
    }
    
    public TrigonometricFunction(IFunction sin, 
                                 IFunction cos,
                                 IFunction tan,
                                 IFunction cot,
                                 IFunction csc) {
        this.sin = sin;
        this.cos = cos;
        this.tan = tan;
        this.cot = cot;
        this.csc = csc;
    }


    @Override
    public double Calculate(double x) 
    {
        double sinX = sin.Calculate(x);
        double cosX = cos.Calculate(x);
        double tanX = tan.Calculate(x);
        double cotX = cot.Calculate(x);
        double cscX = csc.Calculate(x);
        double result = ((((cotX + cotX) / cscX) * cosX) / (sinX * sinX)) + ((cotX * (cosX * tanX)) + sinX);
        if (Settings.needWriteTrigonometricFunc)
        {
            CsvWriter.Write(Settings.trigonometricFuncName, x, result);
        }
        return result;
    }    
}
