package Infrastructure;

public final class Settings 
{   
    private Settings()
    {
    }
    
    public static double tolerance = 1e-10;    
    public static String mainFuncName ="mainFunc.csv";
    public static String logarithmicFuncName = "logFunc.csv";
    public static String trigonometricFuncName = "trigFunc.csv";
    public static String cotName = "cot.csv";
    public static String lnName = "ln.csv";
    public static String log10Name = "log10.csv";
    public static String log5Name = "log5.csv";
    public static String log2Name = "log2.csv";
    public static String cscName = "csc.csv";
    public static String cosName = "cos.csv";
    public static String sinName = "sin.csv";
    public static String tanName = "tan.csv";
    public static boolean needWriteAll = false;
    public static boolean needWriteMainFunc = false;
    public static boolean needWriteLogarithmicFunc = false;
    public static boolean needWriteTrigonometricFunc = false;
    public static boolean needWriteLn = false;
    public static boolean needWriteLog10 = false;
    public static boolean needWriteLog5 = false;
    public static boolean needWriteLog2 = false;
    public static boolean needWriteCot = false;
    public static boolean needWriteCsc = false;
    public static boolean needWriteCos = false;
    public static boolean needWriteSin = false;
    public static boolean needWriteTan = false;
    
    public static void setTolerance(double value)
    {
        tolerance = value;
    }
        
    public static void setSaveAll(boolean needWriteAll) 
    {
        Settings.needWriteAll = needWriteAll;
        if (needWriteAll)
        {
            Settings.needWriteTan = true;
            Settings.needWriteSin = true;
            Settings.needWriteCsc = true;
            Settings.needWriteCos = true;
            Settings.needWriteCot = true;
            Settings.needWriteLog5 = true;
            Settings.needWriteLog2 = true;
            Settings.needWriteLog10 = true;
            Settings.needWriteLn = true;
            Settings.needWriteMainFunc = true;
            Settings.needWriteLogarithmicFunc = true;
            Settings.needWriteTrigonometricFunc = true;
        }
    }
}
