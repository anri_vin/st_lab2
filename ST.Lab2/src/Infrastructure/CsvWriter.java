package Infrastructure;

import java.io.FileWriter;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Locale.Category;

public class CsvWriter 
{
    private static final Locale _fmtLocale = Locale.getDefault(Category.FORMAT);
    private static final NumberFormat _formatter = NumberFormat.getInstance(_fmtLocale);
   
    public static void Write(String fileName, double x, double y)
    {
        try
	{
            try (FileWriter writer = new FileWriter(fileName, true))
            {
                writer.append(_formatter.format(x));
                writer.append("; ");
                writer.append(_formatter.format(y));
                writer.append('\n');
                
                writer.flush();
            }
	}
	catch(IOException e)
	{
	     e.printStackTrace();
	} 
    }    
}
