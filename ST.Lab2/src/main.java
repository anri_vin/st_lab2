import Functions.IFunction;
import Functions.MainFunc;
import Infrastructure.Settings;
import java.io.File;

public class main {
    
    private final static String Usage = "Usage: appName startX endX step"; 
    
    public static void main(String args[])
    {
        if (args.length < 3)
        {
            System.out.println(Usage);
            return;
        }
        try
        {
            double startX = Double.parseDouble(args[0]);
            double endX = Double.parseDouble(args[1]);
            if (startX > endX)
            {
                double temp = endX;
                endX = startX;
                startX = temp;
            }
            double step = Double.parseDouble(args[2]);
            if (step < 0)
            {
                throw new Exception("Step should be a positive number");
            }
            PrepareFiles();
            IFunction function = new MainFunc();
            for (double i=startX; i < endX; i+=step)
            {
                System.out.printf("f(%f) = %f\n", i, function.Calculate(i));
            }
        }
        catch (Exception ex)
        {
            System.err.println("Error: " + ex.getMessage());
        }
    } 
        
    private static void PrepareFiles()
    {
        File file;
        Settings.setSaveAll(true);
        file = new File(Settings.mainFuncName);
        if (file.exists())
        {
            file.delete();
        }
        file = new File(Settings.trigonometricFuncName);
        if (file.exists())
        {
            file.delete();
        }
        file = new File(Settings.logarithmicFuncName);
        if (file.exists())
        {
            file.delete();
        }
        file = new File(Settings.cscName);
        if (file.exists())
        {
            file.delete();
        }
        file = new File(Settings.cosName);
        if (file.exists())
        {
            file.delete();
        }
        file = new File(Settings.cotName);
        if (file.exists())
        {
            file.delete();
        }
        file = new File(Settings.lnName);
        if (file.exists())
        {
            file.delete();
        }
        file = new File(Settings.log10Name);
        if (file.exists())
        {
            file.delete();
        }
        file = new File(Settings.log2Name);
        if (file.exists())
        {
            file.delete();
        }
        file = new File(Settings.log5Name);
        if (file.exists())
        {
            file.delete();
        }
        file = new File(Settings.sinName);
        if (file.exists())
        {
            file.delete();
        }
        file = new File(Settings.tanName);
        if (file.exists())
        {
            file.delete();
        }
    }
}
