package IntegrationTest.ThirdLevel;

import Functions.Base.NaturalLogarithm;
import Functions.Base.Sin;
import Functions.IFunction;
import Functions.LogarithmicFunction;
import Functions.MainFunc;
import Functions.Trigonometric.Cos;
import Functions.Trigonometric.Tan;
import Functions.TrigonometricFunction;
import org.junit.BeforeClass;
import org.junit.Test;
import Stubs.Logarithm.*;
import Stubs.Trigonometric.*;
import static org.junit.Assert.*;

public class TanIntegrationTest {
    
    private static MainFunc mainFunc;
    
    public TanIntegrationTest() {
    }
    
    @BeforeClass
    public static void setUpClass() 
    {
        IFunction logarithmFunc = new LogarithmicFunction(new Log10Stub(),
                                                        new Log5Stub(),
                                                        new Log2Stub(),
                                                        new NaturalLogarithm());
        IFunction trigonometricFunc = new TrigonometricFunction(new Sin(),
                                                                new Cos(),
                                                                new Tan(),
                                                                new CotStub(),
                                                                new CscStub());
        mainFunc = new MainFunc(logarithmFunc,  trigonometricFunc);
    }

    @Test
    public void TestTrigonometricDecreaseHighLeft() 
    {
        assertEquals("0.01",  19999.7, mainFunc.Calculate(0.01), 1E-1);
    }
    
    @Test
    public void TestTrigonometricDecreaseLowLeft()
    {
        assertEquals("0.4", 12.499, mainFunc.Calculate(0.4), 1E-3);
    }
    
    @Test
    public void TestTrigonometricMiddle()
    {
        assertEquals("1.0", 2.206339146, mainFunc.Calculate(1.0), 1E-9);
    }
    
    @Test
    public void TestTrigonometricSecondMiddle()
    {
        assertEquals("2.0", 0.9120514644, mainFunc.Calculate(2.0), 1E-9);
    }
    
    @Test
    public void TestTrigonometricIncreaseLowRight()
    {
        assertEquals("2.8", 15.2154, mainFunc.Calculate(2.8), 1E-4);
    }
    
    @Test
    public void TestTrigonometricIncreaseHighRight()
    {
        assertEquals("3", 97.57866423, mainFunc.Calculate(3.0), 1E-7);
    }
    
    @Test
    public void TestTrigonometricSecondDecreaseHighLeft()
    {
        assertEquals("3.3", 77.2287, mainFunc.Calculate(3.3), 1E-3);
    }
    
    @Test
    public void TestTrigonometricEnd()
    {
        assertEquals("4", 0.08147870438, mainFunc.Calculate(4.0), 1E-9);
    }
    
    @Test
    public void TestLogarithmicStart()
    {
        assertEquals("4.0001", 11.1172, mainFunc.Calculate(4.0001), 1E-4);
    }
    
    @Test
    public void TestLogarithmicIncrease()
    {
        assertEquals("5.0", 25.2038, mainFunc.Calculate(5.0), 1E-4);
    }
    
    @Test
    public void TestLogarithmicMiddle1()
    {
        assertEquals("6.0", 48.677, mainFunc.Calculate(6.0), 1E-3);
    }
    
    @Test
    public void TestLogarithmicMiddle2()
    {
        assertEquals("6.1", 51.5988, mainFunc.Calculate(6.1), 1E-3);
    }
    
    @Test
    public void TestLogarithmicIncreaseHigh()
    {
        assertEquals("10.0", 256.097, mainFunc.Calculate(10.0), 1E-2);
    }
}
