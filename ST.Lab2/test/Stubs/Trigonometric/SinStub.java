package Stubs.Trigonometric;

import java.util.HashMap;
import Functions.IFunction;

public class SinStub implements IFunction 
{
    private HashMap<Double, Double> table;
    
    public SinStub()
    {
        table = new HashMap();
        table.put(0.01, 0.00999983);
        table.put(0.4, 0.389418);
        table.put(1.0, 0.8414709848);
        table.put(2.0, 0.9092974268);
        table.put(2.8, 0.334988);
        table.put(3.0, 0.1411200081);
        table.put(3.3, -0.157746);
        table.put(4.0, -0.7568024953);
    }
    
    @Override
    public double Calculate(double x) 
    {
        return table.get(x);
    }
    
}
