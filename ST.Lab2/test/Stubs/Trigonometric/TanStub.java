package Stubs.Trigonometric;

import java.util.HashMap;
import Functions.IFunction;

public class TanStub implements IFunction 
{
    private HashMap<Double, Double> table;
    
    public TanStub()
    {
        table = new HashMap();
        table.put(0.01, 0.0100003);
        table.put(0.4, 0.422793);
        table.put(1.0, 1.557407725);
        table.put(2.0, -2.185039863);
        table.put(2.8, -0.35553);
        table.put(3.0, -0.1425465431);
        table.put(3.3, 0.159746);
        table.put(4.0, 1.157821282);
    }
    
    @Override
    public double Calculate(double x) 
    {
        return table.get(x);
    }
    
}
