package Stubs.Trigonometric;

import java.util.HashMap;
import Functions.IFunction;

public class TrigonometricFuncStub implements IFunction 
{
    private HashMap<Double, Double> table;
    
    public TrigonometricFuncStub()
    {
        table = new HashMap();
        table.put(0.01, 19999.7);
        table.put(0.4, 12.499);
        table.put(1.0, 2.206339146);
        table.put(2.0, 0.9120514644);
        table.put(2.8, 15.2154);
        table.put(3.0, 97.57866423);
        table.put(3.3, 77.2287);
        table.put(4.0, 0.08147870438);
    }
    
    @Override
    public double Calculate(double x) 
    {
        return table.get((Double)x);
    }
    
}
