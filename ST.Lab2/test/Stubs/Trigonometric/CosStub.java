package Stubs.Trigonometric;

import java.util.HashMap;
import Functions.IFunction;

public class CosStub implements IFunction 
{
    private HashMap<Double, Double> table;
    
    public CosStub()
    {
        table = new HashMap();
        table.put(0.01, 0.99995);
        table.put(0.4, 0.921061);
        table.put(1.0, 0.5403023059);
        table.put(2.0, -0.4161468365);
        table.put(2.8, -0.942222);
        table.put(3.0, -0.9899924966);
        table.put(3.3, -0.98748);
        table.put(4.0, -0.6536436209);
    }
    
    @Override
    public double Calculate(double x) 
    {
        return table.get(x);
    }
    
}
