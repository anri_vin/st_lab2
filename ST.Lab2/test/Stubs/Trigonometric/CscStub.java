package Stubs.Trigonometric;

import java.util.HashMap;
import Functions.IFunction;

public class CscStub implements IFunction 
{
    private HashMap<Double, Double> table;
    
    public CscStub()
    {
        table = new HashMap();
        table.put(0.01, 100.002);
        table.put(0.4, 2.56793);
        table.put(1.0, 1.188395106);
        table.put(2.0, 1.099750170);
        table.put(2.8, 2.98518);
        table.put(3.0, 7.086167396);
        table.put(3.3, -6.33932);
        table.put(4.0, -1.321348709);
    }
    
    @Override
    public double Calculate(double x) 
    {
        return table.get(x);
    }
    
}
