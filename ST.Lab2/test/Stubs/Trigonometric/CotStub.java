package Stubs.Trigonometric;

import java.util.HashMap;
import Functions.IFunction;

public class CotStub implements IFunction 
{
    private HashMap<Double, Double> table;
    
    public CotStub()
    {
        table = new HashMap();
        table.put(0.01, 99.9967);
        table.put(0.4, 2.36522);
        table.put(1.0, 0.6420926159);
        table.put(2.0, -0.4576575544);
        table.put(2.8, -2.8127);
        table.put(3.0, -7.015252551);
        table.put(3.3, 6.25995);
        table.put(4.0, 0.8636911545);
    }
    
    @Override
    public double Calculate(double x) 
    {
        return table.get(x);
    }
    
}
