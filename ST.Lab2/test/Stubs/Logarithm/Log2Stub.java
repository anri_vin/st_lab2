package Stubs.Logarithm;

import java.util.HashMap;
import Functions.IFunction;

public class Log2Stub implements IFunction 
{
    private HashMap<Double, Double> table;
    
    public Log2Stub()
    {
        table = new HashMap();
        table.put(4.0001, 2.00004);
        table.put(5.0, 2.32193);
        table.put(6.0, 2.58496);
        table.put(6.1, 2.60881);
        table.put(10.0, 3.32193);
    } 
    
    @Override
    public double Calculate(double x) 
    {
        return table.get(x);
    }
    
}
