package Stubs.Logarithm;

import java.util.HashMap;
import Functions.IFunction;

public class LogarithmFuncStub implements IFunction 
{
    private HashMap<Double, Double> table;
    
    public LogarithmFuncStub()
    {
        table = new HashMap();
        table.put(4.0001, 11.1172);
        table.put(5.0, 25.2038);
        table.put(6.0, 48.677);
        table.put(6.1, 51.5988);
        table.put(10.0, 256.097);
    }
    
    @Override
    public double Calculate(double x) 
    {
        return table.get((Double)x);
    }
    
}
