package Stubs.Logarithm;

import java.util.HashMap;
import Functions.IFunction;

public class Log10Stub implements IFunction 
{
    private HashMap<Double, Double> table;
    
    public Log10Stub()
    {
        table = new HashMap();
        table.put(4.0001, 0.602071);
        table.put(5.0, 0.69897);
        table.put(6.0, 0.778151);
        table.put(6.1, 0.78533);
        table.put(10.0, 1.0);
    }
    
    @Override
    public double Calculate(double x) 
    {
        return table.get(x);
    }
    
}
