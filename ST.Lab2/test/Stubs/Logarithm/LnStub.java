package Stubs.Logarithm;

import java.util.HashMap;
import Functions.IFunction;

public class LnStub implements IFunction 
{
    private HashMap<Double, Double> table;
    
    public LnStub()
    {
        table = new HashMap();
        table.put(4.0001, 1.38632);
        table.put(5.0, 1.60944);
        table.put(6.0, 1.79176);
        table.put(6.1, 1.80829);
        table.put(10.0, 2.30259);
    }
    
    @Override
    public double Calculate(double x) 
    {
        System.out.println(x);
        return table.get((Double)x);
    }
    
}
