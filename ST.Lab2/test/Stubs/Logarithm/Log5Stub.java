package Stubs.Logarithm;

import java.util.HashMap;
import Functions.IFunction;

public class Log5Stub implements IFunction 
{
    private  HashMap<Double, Double> table;
    
    public Log5Stub()
    {
        table = new HashMap();
        table.put(4.0001, 0.861369);
        table.put(5.0, 1.0);
        table.put(6.0, 1.11328);
        table.put(6.1, 1.12355);
        table.put(10.0, 1.43068);
    }
    
    @Override
    public double Calculate(double x) 
    {
        return table.get(x);
    }
    
}
